import { Temp } from "../temp/temp.entity";

export interface Fermentation {
    temp: Temp;
}
