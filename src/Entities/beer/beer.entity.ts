import { Ingredients } from "../ingredients/ingredients.entity";
import { Method } from "../method/method.entity";

/* istanbul ignore file */
export interface Beer {
    id: number;
    srm: number;
    ph: number;
    image_url: string;
    name: string;
    tagline: string;
    description: string;
    brewers_tips: string;
    food_pairing: string[];
    first_brewed: string;
    ingredients: Ingredients;
    method: Method;
}
