import { Fermentation } from "../fermentation/fermentation";
import { Mash } from "../mash/mash.entity";

export interface Method {
    mash_temp: Mash[];
    fermentation: Fermentation;
}
