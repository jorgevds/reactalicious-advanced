import { Temp } from "../temp/temp.entity";

export interface Mash {
    duration: number;
    temp: Temp;
}
