import { Hops } from "../hops/hops.entity";
import { Malt } from "../malt/malt.entity";

export interface Ingredients {
    hops: Hops[];
    malt: Malt[];
    yeast: string;
}
