import { Amount } from "../amount/amount.entity";

export interface Hops {
    name: string;
    amount: Amount;
    add: string;
    attribute: string;
}
