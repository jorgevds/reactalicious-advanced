import { Amount } from "../amount/amount.entity";

export interface Malt {
    name: string;
    amount: Amount;
}
