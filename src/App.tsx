import { DataWrapper } from "src/Components/App/DataWrapper";

import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { persistor, store } from "src/state-management";

const App = () => (
    <Provider store={store}>
        <PersistGate persistor={persistor}>
            <DataWrapper />
        </PersistGate>
    </Provider>
);

export default App;
