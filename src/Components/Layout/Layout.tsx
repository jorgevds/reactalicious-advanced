import React from "react";
import { Footer, Header } from "../Navigation/index";

import { navigation } from "src/Data/navigation-data";

const Layout: React.FC = ({ children }) => (
    <>
        <Header navigation={navigation} />
        {children}
        <Footer navigation={navigation} />
    </>
);
export default Layout;
