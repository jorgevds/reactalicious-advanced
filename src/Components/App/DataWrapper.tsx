import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { beerActions, BeerState, RootState } from "src/state-management";

import { BrowserRouter, Route, Switch } from "react-router-dom";
import { BeerDetail, BeerOverview } from "./Beer/index";

export const DataWrapper: React.FC = () => {
    const dispatch = useDispatch();
    const beerList: BeerState = useSelector((state: RootState) => state.beerReducer);

    useEffect(() => {
        if (beerList.beers.length === 0) {
            dispatch(beerActions.LIST.call(this));
        }

        return () => {};
    }, [dispatch, beerList]);

    const handleClick = () => {
        dispatch(beerActions.RANDOM.call(this));
    };

    return beerList ? (
        <article data-testid="data">
            <BrowserRouter>
                <Switch>
                    <Route exact={true} path="/">
                        <h1 style={{ textAlign: "center" }}>Reactalicious</h1>
                        <BeerOverview beerList={beerList.beers} handleClick={handleClick} />
                    </Route>
                    <Route path="/:beerId">
                        <BeerDetail beerList={beerList.beers} />
                    </Route>
                </Switch>
            </BrowserRouter>
        </article>
    ) : (
        <article style={{ flexGrow: 1 }}>
            <h1>Loading... Please hold</h1>
        </article>
    );
};
