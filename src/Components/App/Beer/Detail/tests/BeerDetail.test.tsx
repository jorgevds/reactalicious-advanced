import { cleanup, fireEvent, render } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";

import { dummyBeerList } from "src/Data/testing";
import { BeerDetail } from "../BeerDetail";

afterEach(cleanup);

window.scrollTo = jest.fn();

test("Component renders", () => {
    const { getByTestId } = render(
        <Router>
            <BeerDetail beerList={dummyBeerList.beers} />
        </Router>
    );

    const component = getByTestId("beerDetail");
    expect(component).toBeInTheDocument();
});

test("Prop beerList is not null", () => {
    render(
        <Router>
            <BeerDetail beerList={dummyBeerList.beers} />
        </Router>
    );

    expect(dummyBeerList).not.toBe(null);
});

test("ScrollToTop fn works", () => {
    const { getByText } = render(
        <Router>
            <BeerDetail beerList={dummyBeerList.beers} />
        </Router>
    );

    fireEvent.click(getByText(/back to overview/i));
    expect(window.scrollTo).toHaveBeenCalledTimes(1);
});
