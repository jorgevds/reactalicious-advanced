import { Link } from "react-router-dom";
import { Beer } from "src/Entities";
import { NewBeer } from "../New/NewBeer";
import styles from "./BeerOverview.module.css";

export interface BeerOverviewProps {
    beerList: Beer[];
    handleClick: () => void;
}

export const BeerOverview: React.FC<BeerOverviewProps> = ({ beerList, handleClick }) => (
    <section data-testid="overview">
        <ul className={styles.flex}>
            {beerList && beerList?.length > 0 ? (
                beerList.map((beer: Beer) => (
                    <li key={beer.id} className={styles.card}>
                        <article>
                            <div className={styles.imgWrapper}>
                                <img src={beer.image_url} className={styles.img} height="250px" alt={beer.name} />
                            </div>
                            <h1 className={styles.h1}>{beer.name}</h1>
                            <p className={styles.tagline}>{beer.tagline}</p>
                            <p className={styles.truncate}>{beer.description}</p>
                            <Link to={`/${beer.id}`}>
                                <button>Read more</button>
                            </Link>
                        </article>
                    </li>
                ))
            ) : (
                <></>
            )}
            <li className={styles.card} data-testid="newBeerLI">
                <NewBeer handleClick={handleClick} />
            </li>
        </ul>
    </section>
);
