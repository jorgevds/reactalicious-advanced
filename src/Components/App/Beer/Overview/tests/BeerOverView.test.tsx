import { cleanup, render } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";
import { dummyBeerList, dummyClick } from "src/Data/testing";
import { BeerOverview } from "../BeerOverview";

afterEach(cleanup);

test("Component renders", () => {
    const { getByTestId } = render(
        <Router>
            <BeerOverview beerList={dummyBeerList.beers} handleClick={dummyClick} />
        </Router>
    );

    const overview = getByTestId("overview");
    expect(overview).toBeInTheDocument();
});

test("Renders the NewBeer component", () => {
    const { getByTestId } = render(
        <Router>
            <BeerOverview beerList={dummyBeerList.beers} handleClick={dummyClick} />
        </Router>
    );

    const newBeer = getByTestId("newBeerLI");
    expect(newBeer).toBeInTheDocument();
});

test("Prop beerList is not null", () => {
    render(
        <Router>
            <BeerOverview beerList={dummyBeerList.beers} handleClick={dummyClick} />
        </Router>
    );

    expect(dummyBeerList).not.toBe(null);
});

test("Prop handleClick is not null", () => {
    render(
        <Router>
            <BeerOverview beerList={dummyBeerList.beers} handleClick={dummyClick} />
        </Router>
    );

    expect(dummyClick).not.toBe(null);
});
