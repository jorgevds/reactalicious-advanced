import question from "src/Assets/Images/QuestionMark.png";
import styles from "./NewBeer.module.css";

export interface NewBeerProps {
    handleClick: () => void;
}

export const NewBeer: React.FC<NewBeerProps> = ({ handleClick }) => {
    return (
        <article data-testid="newBeer">
            <div className={styles.imgWrapper}>
                <img src={question} className={styles.img} height="200px" alt="Mystery beer" />
            </div>
            <h1 className={styles.h1}>Mystery beer</h1>
            <p className={styles.tagline}>Your beer here?</p>

            <button onClick={handleClick}>Click me</button>
        </article>
    );
};
