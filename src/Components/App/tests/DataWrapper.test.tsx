import { cleanup, render } from "@testing-library/react";
import { createMemoryHistory } from "history";
import { Provider } from "react-redux";
import { Route, Router, Switch } from "react-router-dom";
import { dummyClick } from "src/Data/testing";
import { dummyBeerList } from "src/Data/testing/mocks/beerlist-data";
import { store } from "src/state-management";

import { BeerDetail, BeerOverview } from "../Beer/index";
import { DataWrapper } from "../DataWrapper";

afterEach(cleanup);

const history = createMemoryHistory();

test("Component renders", () => {
    const { queryByTestId } = render(
        <Provider store={store}>
            <DataWrapper />
        </Provider>
    );

    const dataWrapper = queryByTestId("data");
    expect(dataWrapper).toBeInTheDocument();
}); // So this one only took 2 whole hours lmao

test("Fetch function returns data", () => {
    const { queryByText } = render(
        <Provider store={store}>
            <DataWrapper />
        </Provider>
    );

    const reactalicious = queryByText(/reactalicious/i);
    expect(reactalicious).toBeInTheDocument();

    const loadingScreen = queryByText(/loading/i);
    expect(loadingScreen).not.toBeInTheDocument();
});

test("Router renders the home page correctly", () => {
    const { queryByTestId } = render(
        <Router history={history}>
            <BeerOverview beerList={dummyBeerList.beers} handleClick={dummyClick} />
        </Router>
    );

    const backButton = queryByTestId("backButton");
    expect(backButton).not.toBeInTheDocument();
});

test("Router renders the detail page correctly", () => {
    history.push("/1");

    const { queryByTestId } = render(
        <Router history={history}>
            <Switch>
                <Route path="/:beerId">
                    <BeerDetail beerList={dummyBeerList.beers} />
                </Route>
            </Switch>
        </Router>
    );

    const backButton = queryByTestId("backButton");
    expect(backButton).toBeInTheDocument();
});
