import { cleanup, render } from "@testing-library/react";
import { dummyNavigation } from "../../Header/tests/Header.test";
import { Footer } from "../Footer";

afterEach(cleanup);

test("Component renders", () => {
    const { getByText } = render(<Footer navigation={dummyNavigation} />);

    const footer = getByText("test");
    expect(footer).toBeInTheDocument();
});
