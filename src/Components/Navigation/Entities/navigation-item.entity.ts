export interface NavigationItem {
    id: number;
    name: string;
    href: string;
}
