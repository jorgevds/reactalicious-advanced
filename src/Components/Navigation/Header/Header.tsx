import React from "react";
import { NavigationItem } from "../Entities/navigation-item.entity";
import styles from "./Header.module.css";

interface HeaderProps {
    navigation: NavigationItem[];
}

export const Header: React.FC<HeaderProps> = ({ navigation }) => (
    <header className={styles.navbar}>
        <nav>
            <ul className={styles.navList}>
                {navigation.map((nav: NavigationItem) => (
                    <li key={`header nav item with key ${nav.id}`}>
                        <a href={nav.href}>{nav.name}</a>
                    </li>
                ))}
            </ul>
        </nav>
    </header>
);
