import { cleanup, render } from "@testing-library/react";
import { NavigationItem } from "../../Entities/navigation-item.entity";
import { Header } from "../Header";

afterEach(cleanup);

export const dummyNavigation: NavigationItem[] = [
    {
        id: 1,
        name: "test",
        href: "",
    },
];

test("Component renders", () => {
    const { getByText } = render(<Header navigation={dummyNavigation} />);

    const header = getByText("test");
    expect(header).toBeInTheDocument();
});
