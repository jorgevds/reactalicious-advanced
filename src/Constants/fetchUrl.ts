/* istanbul ignore file */

export enum URLTypes {
    RANDOM = "RANDOM",
    BASE = "BASE",
}

export const beerUrls: Record<URLTypes, string> = {
    [URLTypes.BASE]: "https://api.punkapi.com/v2/beers?hops=cascade",
    [URLTypes.RANDOM]: "https://api.punkapi.com/v2/beers/random",
};
