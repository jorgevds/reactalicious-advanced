import { Beer } from "src/Entities";

export interface BeerState {
    loadingBeer: boolean;
    successBeer: boolean;
    failedBeer: boolean;
    beers: Beer[];
    error: string;
}

export enum BeerTypes {
    LOADING_BEER = "LOADING BEER",
    SUCCESS_BEER = "SUCCESS BEER",
    FAILED_BEER = "FAILED BEER",
}
