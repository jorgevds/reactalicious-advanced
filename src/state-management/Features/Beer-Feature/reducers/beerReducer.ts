import produce from "immer";
import { AnyAction } from "redux";
import { BeerState, BeerTypes } from "../Entities/state.entities";

export const initialState: BeerState = {
    loadingBeer: true,
    successBeer: false,
    failedBeer: false,
    beers: [],
    error: "",
};

export const beerReducer = (previousState = initialState, action: AnyAction): BeerState => {
    return produce(previousState, newState => {
        switch (action.type) {
            case BeerTypes.LOADING_BEER: {
                newState.loadingBeer = true;
                newState.successBeer = initialState.successBeer;
                newState.beers = initialState.beers;
                newState.failedBeer = initialState.failedBeer;

                return newState;
            }
            case BeerTypes.SUCCESS_BEER: {
                newState.loadingBeer = false;
                newState.successBeer = true;
                newState.beers = action.beers;

                return newState;
            }
            case BeerTypes.FAILED_BEER: {
                newState.loadingBeer = false;
                newState.failedBeer = true;
                newState.error = action.error;

                return newState;
            }
            default: {
                return previousState ?? initialState;
            }
        }
    });
};
