import { beerUrls, URLTypes } from "src/Constants/fetchUrl";
import { Beer } from "src/Entities";
import { fetchItems } from "src/Utils/fetch";
import { AppDispatch, store } from "../../../Application-State/Store/store";
import { BeerTypes } from "../Entities/state.entities";

export enum BeerActions {
    LIST = "LIST",
    RANDOM = "RANDOM",
}

type PerformedBeerAction = () => (dispatch: AppDispatch) => Promise<void>;

const getBeerList: () => (dispatch: AppDispatch) => Promise<void> =
    () =>
    async (dispatch: AppDispatch): Promise<void> => {
        const type: URLTypes = URLTypes.BASE;
        dispatch({ type: BeerTypes.LOADING_BEER });

        try {
            const beers: Beer[] = await fetchItems<Beer>(beerUrls[type]);

            dispatch({ type: BeerTypes.SUCCESS_BEER, beers });
        } catch (error: any) {
            dispatch({ type: BeerTypes.FAILED_BEER, error: error.message });
        }
    };

const getRandomBeer: () => (dispatch: AppDispatch) => Promise<void> =
    () =>
    async (dispatch: AppDispatch): Promise<void> => {
        const type: URLTypes = URLTypes.RANDOM;
        dispatch({ type: BeerTypes.LOADING_BEER });

        try {
            const randomBeers: Beer[] = await fetchItems<Beer>(beerUrls[type], 1);
            const beers: Beer[] = [...store.getState().beerReducer.beers, ...randomBeers];

            dispatch({ type: BeerTypes.SUCCESS_BEER, beers });
        } catch (error: any) {
            dispatch({ type: BeerTypes.FAILED_BEER, error: error.message });
        }
    };

export const beerActions: Record<BeerActions, PerformedBeerAction> = {
    [BeerActions.LIST]: getBeerList,
    [BeerActions.RANDOM]: getRandomBeer,
};
