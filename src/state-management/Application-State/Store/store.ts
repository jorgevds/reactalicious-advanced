// import {configurestore} from "@reduxjs/toolkit"

import { applyMiddleware, createStore } from "redux";

import thunk from "redux-thunk";

import { persistReducer, persistStore } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { rootReducer } from "../rootReducer";

const middleware = [thunk];

if (process.env.NODE_ENV === "development") {
    const { logger } = require("redux-logger");
    middleware.push(logger);
}

const persistConfig = {
    key: "root",
    storage,
    whitelist: ["beerReducer"],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer, applyMiddleware(...middleware));

const persistor = persistStore(store);

export { store, persistor };

export type AppDispatch = typeof store.dispatch;
