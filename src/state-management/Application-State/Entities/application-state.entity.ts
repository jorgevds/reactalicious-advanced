import { BeerState } from "../../Features/index";

export interface ApplicationState {
    beerReducer: BeerState;
}
