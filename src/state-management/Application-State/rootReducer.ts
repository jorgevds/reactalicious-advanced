import { AnyAction, CombinedState, combineReducers, Reducer } from "redux";
import { beerReducer } from "../Features/Beer-Feature/reducers/beerReducer";
import { ApplicationState } from "./Entities/application-state.entity";

export const rootReducer: Reducer<CombinedState<ApplicationState>, AnyAction> = combineReducers({
    beerReducer: beerReducer,
});

export type RootState = ReturnType<Reducer<CombinedState<ApplicationState>>>;
