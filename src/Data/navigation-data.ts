import { NavigationItem } from "../Components/Navigation/Entities/navigation-item.entity";

export const navigation: NavigationItem[] = [
    {
        id: 1,
        name: "Home",
        href: "/",
    },
    {
        id: 2,
        name: "Contact",
        href: "/contact",
    },
];
