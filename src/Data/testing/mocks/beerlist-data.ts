import { BeerState } from "src/state-management";

export const dummyBeerList: BeerState = {
    loadingBeer: true,
    successBeer: false,
    failedBeer: false,
    error: "",
    beers: [
        {
            id: 1,
            srm: 10,
            ph: 4.4,
            image_url: "https://images.punkapi.com/v2/keg.png",
            name: "beer",
            tagline: "A Real Bitter Experience.",
            description:
                "A light, crisp and bitter IPA brewed with English and American hops. A small batch brewed only once.",
            brewers_tips:
                "The earthy and floral aromas from the hops can be overpowering. Drop a little Cascade in at the end of the boil to lift the profile with a bit of citrus.",
            food_pairing: ["Spicy chicken massala"],
            first_brewed: "09/2007",
            ingredients: {
                hops: [
                    {
                        name: "Fuggles",
                        amount: { unit: "grams", value: 25 },
                        add: "start",
                        attribute: "bitter",
                    },
                ],
                malt: [{ name: "Caramalt", amount: { unit: "kilograms", value: 0.2 } }],
                yeast: "Wyeast 1056",
            },
            method: {
                mash_temp: [
                    {
                        duration: 75,
                        temp: {
                            value: 64,
                            unit: "celsius",
                        },
                    },
                ],
                fermentation: { temp: { value: 19, unit: "celsius" } },
            },
        },
    ],
};
