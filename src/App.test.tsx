import { cleanup, render } from "@testing-library/react";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import App from "./App";
import { DataWrapper } from "./Components/App/DataWrapper";
import { persistor, store } from "./state-management/Application-State/Store/store";

afterEach(cleanup);

test("App renders", () => {
    render(<App />);
});

test("App renders with data", () => {
    const { queryByTestId } = render(
        <Provider store={store}>
            <PersistGate persistor={persistor}>
                <DataWrapper />
            </PersistGate>
        </Provider>
    );
    const app = queryByTestId("data");
    expect(app).toBeInTheDocument();
});

test("App store is not null", () => {
    render(<Provider store={store} />);

    expect(store).not.toBe(null);
});

test("App persistor is not null", () => {
    render(
        <Provider store={store}>
            <PersistGate persistor={persistor} />
        </Provider>
    );

    expect(persistor).not.toBe(null);
});
