/* istanbul ignore file */
import axios, { AxiosResponse } from "axios";
// TODO: add react query to this
export const fetchItems = async <T>(url: string, numberToFetch: number = 3): Promise<T[]> => {
    const response: AxiosResponse<any> = await axios.get(url, {});
    return (await response.data.slice(0, numberToFetch)) as T[];
};
