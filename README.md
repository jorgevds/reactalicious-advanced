# Reactalicious advanced

Welcome to the wonderfully over-engineered Reactalicious repo.

The original Reactalicious repo has been reorganized and over-engineered for demonstration purposes. While in reality
you probably won't want to plan **this** much ahead, this kind of structure and code is not unusual in a production
application. As such, this codebase is a minor demonstration--a taste--of how cryptic and opinionated a project can be.
